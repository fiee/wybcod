package threadtest;

/**
 * @ClassName: MyThread03
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-19 22:38
 * @Version: 1.0.0
 */
public class MyThread03 extends Thread {

    public int count = 5;
    public MyThread03(String name){
        super();
        this.setName(name);
    }

    @Override
    public void run(){
        super.run();
        while (count>0){
            count--;
            System.out.println("由"+this.currentThread().getName()+"计算，count="+count);
        }
    }
}
