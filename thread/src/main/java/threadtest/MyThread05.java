package threadtest;

/**
 * @ClassName: MyThread05
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 19:00
 * @Version: 1.0.0
 */
public class MyThread05 extends Thread {
    private int i=5;
    @Override
    public void run(){
        synchronized (this){
            System.out.println("当前线程名称="+Thread.currentThread().getName()+",i="+(i--));
        }
    }
}
