package threadtest;

/**
 * @ClassName: MyThread07
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 19:10
 * @Version: 1.0.0
 */
public class MyThread07 extends Thread {
    public MyThread07(){
        System.out.println("构造方法-----开始");
        System.out.println("Thread.currentThread().getName()="+Thread.currentThread().getName());
        System.out.println("this.getName()="+this.getName());
        System.out.println("构造方法-----结束");
    }

    @Override
    public void run(){
        System.out.println("run方法----开始");
        System.out.println("Thread.currentThread().getName()="+Thread.currentThread().getName());
        System.out.println("this.getName()="+this.getName());
        System.out.println("run方法----结束");
    }
}
