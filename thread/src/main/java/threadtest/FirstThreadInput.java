package threadtest;

/**
 * @ClassName: FirstThreadInput
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-22 21:14
 * @Version: 1.0.0
 */
public class FirstThreadInput extends Thread {
    @Override
    public void run(){
        System.out.println("调用FirstThreadInput类的run（）重写方法");
        for (int i = 0; i < 5 ; i++) {
            System.out.println("FirstThreadInput线程中i="+i);
            try {
                Thread.sleep((long) (Math.random()*100));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
