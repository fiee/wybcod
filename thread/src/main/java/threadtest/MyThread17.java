package threadtest;

/**
 * @ClassName: MyThread17
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 20:11
 * @Version: 1.0.0
 */
public class MyThread17 extends Thread {
    @Override
    public void run(){
        super.run();
        try {
            for (int i = 0; i < 1000 ; i++) {
                System.out.println("i="+(i+1));
            }
            System.out.println("run begin");
            Thread.sleep(200);
            System.out.println("run end");
        } catch (InterruptedException e) {
            System.out.println("先停止了，在遇到sleep！进入catch！");
            e.printStackTrace();
        }
    }
}
