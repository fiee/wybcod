package threadtest;

/**
 * @ClassName: MyThread21
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-21 20:30
 * @Version: 1.0.0
 */
public class MyThread21 extends Thread {
    private long i = 0;

    public long getI() {
        return i;
    }

    public void setI(long i) {
        this.i = i;
    }

    @Override
    public void run(){
        while (true){
            i++;
        }
    }
}
