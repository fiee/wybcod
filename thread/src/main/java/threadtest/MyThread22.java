package threadtest;

/**
 * @ClassName: MyThread22
 * @Description: TODO
 * @Author: wwangyb
 * @Date: 2019-07-22 21:02
 * @Version: 1.0.0
 */
public class MyThread22 extends Thread {
    private long i = 0;
    @Override
    public void run(){
        while (true){
            i++;
            System.out.println(i);
        }
    }
}
