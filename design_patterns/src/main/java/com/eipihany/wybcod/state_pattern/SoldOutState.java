package com.eipihany.wybcod.state_pattern;

/**
 * @ClassName: SoldOutState
 * @Description: 售尽状态
 * @Author: wwangyb
 * @Date: 2020-08-06 21:44
 * @Version: 1.0.0
 */
public class SoldOutState extends State {
    GumballMachine gumballMachine;
    public SoldOutState(GumballMachine gumballMachine){
        this.gumballMachine = gumballMachine;
    }
    @Override
    public void insertQuarter() {
        System.out.println("糖果已经售尽");
        returnQuarter();
    }

    @Override
    public void ejectQuarter() {
        System.out.println("没有投币，无法退币");
    }

    @Override
    public void turnCrank() {
        System.out.println("糖果已经售尽");
    }

    @Override
    public void dispense() {
        System.out.println("糖果已经售尽");
    }
}
