package com.eipihany.freemarker.controller;

import com.eipihany.freemarker.bean.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: UserController
 * @Description: TODO
 * @Author: epiphany
 * @Date: 2020-08-30 19:53
 * @Version: 1.0.0
 */
@Controller
public class UserController {

     static final Integer NUM = 10;

    @GetMapping("/user")
    public String user(Model model){
        List<User> users = new ArrayList<>();
        for (int i = 0; i < NUM; i++) {
            User user = new User();
            user.setId((long) i);
            user.setUsername("epiphany>>>"+i);
            user.setAddress("com.epiphany>>>"+i);
            users.add(user);
        }
        model.addAttribute("users",users);
        return "user";
    }
}
